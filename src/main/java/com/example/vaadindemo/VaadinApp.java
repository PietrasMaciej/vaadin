package com.example.vaadindemo;

import com.example.vaadindemo.domain.Bike;
import com.example.vaadindemo.service.BikeManager;
import com.vaadin.annotations.Title;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@Title("Vaadin Demo App")
public class VaadinApp extends UI {

	private static final long serialVersionUID = 1L;
	
			@Override
			protected void init(VaadinRequest request) {
			
			Bike bike = new Bike("Romet", 1982, "Konsul", "Miejski");
			BeanItem<Bike> bikeItem = new BeanItem<Bike>(bike);
			
			FormLayout form = new FormLayout();
			FieldGroup binder = new FieldGroup(bikeItem);

			binder.setItemDataSource(bikeItem);
			
			form.addComponent(binder.buildAndBind("Model", "model"));
			form.addComponent(binder.buildAndBind("Rok produkcji", "yearOfProduction"));
			form.addComponent(binder.buildAndBind("Marka", "brand"));
			form.addComponent(binder.buildAndBind("Typ", "type"));
			

			binder.setBuffered(true);

			binder.getField("model").setRequired(true);
			binder.getField("brand").setRequired(true);

			VerticalLayout fvl = new VerticalLayout();
			fvl.setMargin(true);
			fvl.addComponent(form);

			setContent(fvl);

			
	}

}
