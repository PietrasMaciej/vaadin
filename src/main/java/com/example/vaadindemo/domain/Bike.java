package com.example.vaadindemo.domain;

import java.util.UUID;

public class Bike {
	
	private UUID id;
	
	private String brand;
	
	private int yearOfProduction;
	
	private String model;
	
	private String type;
	
	public Bike(String brand, int yearOfProduction, String model, String type) {
		super();
		this.brand = brand;
		this.yearOfProduction = yearOfProduction;
		this.model = model;
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Bike() {
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public int getYearOfProduction() {
		return yearOfProduction;
	}

	public void setYearOfProduction(int yearOfProduction) {
		this.yearOfProduction = yearOfProduction;
	}
	
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Override
	public String toString() {
		return "Bike [brand=" + brand + ", yearOfProduction=" + yearOfProduction
				+ ", model=" + model + "]";
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
	
}
