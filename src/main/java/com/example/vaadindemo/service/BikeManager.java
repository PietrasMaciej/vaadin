package com.example.vaadindemo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.example.vaadindemo.domain.Bike;

public class BikeManager {
	
	private List<Bike> db = new ArrayList<Bike>();
	
	public void addBike(Bike bike){
		Bike b = new Bike(bike.getBrand(), bike.getYearOfProduction(), bike.getModel(), bike.getType());
		b.setId(UUID.randomUUID());
		db.add(b);
	}
	
	public List<Bike> findAll(){
		return db;
	}

	public void delete(Bike bike) {
		
		Bike toRemove = null;
		for (Bike b: db) {
			if (b.getId().compareTo(bike.getId()) == 0){
				toRemove = b;
				break;
			}
		}
		db.remove(toRemove);		
	}

	public void updateBike(Bike bike) {
		// TODO DOIT YOURSELF
		
	}

}
